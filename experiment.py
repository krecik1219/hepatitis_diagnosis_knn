from dataclasses import dataclass

import numpy as np
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.neighbors import KNeighborsClassifier


@dataclass
class SingleResult:
    accuracy: float
    prediction: np.ndarray
    y_test: np.ndarray

    def __str__(self) -> str:
        return f'accuracy: {self.accuracy}'

    def __repr__(self) -> str:
        return f'SingleResult(accuracy: {self.accuracy!r})\n' \
               f'prediction: {self.prediction!r}\n' \
               f'y_test: {self.y_test!r}'


class AvgResult:
    def __init__(self, metric, k, features_num, single_results):
        self.metric = metric
        self.k = k
        self.features_num = features_num
        self._single_results = sorted(single_results, key=lambda x: x.accuracy, reverse=True)
        self.avg_accuracy = 0.0
        for single_result in self._single_results:
            self.avg_accuracy += single_result.accuracy
        self.avg_accuracy /= len(self._single_results)

    def get_best_single_result(self):
        return self._single_results[0]

    def __str__(self) -> str:
        return f'metric: {self.metric}, k: {self.k}, features_num: {self.features_num}, accuracy: {self.avg_accuracy}'

    def __repr__(self) -> str:
        return f'AvgResult(metric: {self.metric!r}, k: {self.k!r}, features_num: {self.features_num!r}, ' \
               f'accuracy: {self.avg_accuracy!r})'


class ExperimentResult:
    def __init__(self, avg_results):
        self.avg_results = sorted(avg_results, key=lambda x: x.avg_accuracy, reverse=True)
        best_single_accuracy = 0
        self._best_single_result = None
        for avg_result in self.avg_results:
            if avg_result.get_best_single_result().accuracy > best_single_accuracy:
                best_single_accuracy = avg_result.get_best_single_result().accuracy
                self._best_single_result = avg_result

    def get_best_avg_result(self):
        return self.avg_results[0]

    def get_best_single_result(self):
        return self._best_single_result


class Experiment:
    def __init__(self, metrics, k, splits, repeats, features_selector):
        self._metrics = metrics
        self._k = k
        self._splits = splits
        self._repeats = repeats
        self._features_selector = features_selector

    def run(self, num_of_features, seed=None):
        dataset = self._features_selector.get_transformed_dataset(num_of_features)
        avg_results = []
        for metric in self._metrics:
            for k in self._k:
                rskf = RepeatedStratifiedKFold(n_splits=self._splits, n_repeats=self._repeats, random_state=seed)
                single_results = []
                for train_index, test_index in rskf.split(dataset.data_features, dataset.data_class):
                    X_train, X_test = dataset.data_features[train_index], dataset.data_features[test_index]
                    y_train, y_test = dataset.data_class[train_index], dataset.data_class[test_index]
                    metric_params = None
                    if metric == 'seuclidean':
                        metric_params = {'V': np.var(X_train, axis=0)}
                    classifier = KNeighborsClassifier(metric=metric, n_neighbors=k, metric_params=metric_params)
                    classifier.fit(X_train, y_train)
                    prediction = classifier.predict(X_test)
                    accuracy = (np.sum(prediction == y_test) / float(np.shape(y_test)[0]))
                    single_results.append(SingleResult(accuracy, prediction, y_test))

                avg_results.append(AvgResult(metric, k, num_of_features, single_results))

        return avg_results
