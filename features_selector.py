import numpy as np
from sklearn.feature_selection import SelectKBest, chi2

from dataset import DataSet


class FeaturesSelector(object):
    def __init__(self, original_dataset):
        self.original_dataset = original_dataset
        self.sorted_values = np.zeros(np.shape(self.original_dataset.data_features)[1])
        self.sorted_labels_numbers = np.arange(0, np.shape(self.original_dataset.data_features)[1])

    def rank_features(self):
        k_best_selector = SelectKBest(chi2, k='all')
        k_best_fit = k_best_selector.fit(self.original_dataset.data_features, self.original_dataset.data_class)
        self.sorted_values = np.sort(k_best_fit.scores_)[::-1]
        self.sorted_labels_numbers = np.argsort(k_best_fit.scores_)[::-1]

    def get_sorted_labels(self, num_of_best_features='all'):
        if num_of_best_features == 'all':
            num_of_best_features = np.shape(self.sorted_labels_numbers)[0]

        return self.original_dataset.features_labels[self.sorted_labels_numbers[:num_of_best_features]]

    def get_transformed_dataset(self, num_of_best_features):
        class_label = self.original_dataset.class_label
        features_labels = self.get_sorted_labels(num_of_best_features)
        data_class = self.original_dataset.data_class
        if num_of_best_features == 'all':
            num_of_best_features = np.shape(self.sorted_labels_numbers)[0]

        data_features = self.original_dataset.data_features[:, self.sorted_labels_numbers[:num_of_best_features]]
        return DataSet(class_label, features_labels, data_class, data_features)
