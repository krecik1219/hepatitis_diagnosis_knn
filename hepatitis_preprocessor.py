from dataset import DataSet


class HepatitisPreprocessor(object):
    BINARY_CATEGORICAL_FEATURES_INDICES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 18]

    def preprocess(self, dataset: DataSet):
        self._make_values_of_binary_categories_0_and_1(dataset)

    def _make_values_of_binary_categories_0_and_1(self, dataset):
        dataset.data_features[:, HepatitisPreprocessor.BINARY_CATEGORICAL_FEATURES_INDICES] -= 1
        dataset.data_class -= 1
