import numpy as np

from dataset import DataSet


class DataLoader(object):
    def __init__(self, data_path, labels_path):
        self.data_path = data_path
        self.labels_path = labels_path

    def load(self, class_column):
        class_label, features_labels = self._load_labels(class_column)
        data_class, data_features = self._load_data(class_column)

        return DataSet(class_label, features_labels, data_class, data_features)

    def _load_labels(self, class_column):
        labels = np.loadtxt(self.labels_path, dtype=str, delimiter='\n')
        return labels[class_column], np.delete(labels, class_column)

    def _load_data(self, class_column):
        data = np.genfromtxt(self.data_path, missing_values='?', delimiter=',')

        return data[:, class_column], np.delete(data, class_column, axis=1)
