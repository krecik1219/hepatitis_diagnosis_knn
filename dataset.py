from dataclasses import dataclass

import numpy as np

from sklearn.impute import SimpleImputer


@dataclass
class DataSet(object):
    class_label: str
    features_labels: np.ndarray
    data_class: np.ndarray
    data_features: np.ndarray

    def impute_missing_values(self):
        imputer_most_freq = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
        self.data_features[:, 0:13] = imputer_most_freq.fit_transform(self.data_features[:, 0:13])
        self.data_features[:, 14:16] = imputer_most_freq.fit_transform(self.data_features[:, 14:16])
        self.data_features[:, 17:19] = imputer_most_freq.fit_transform(self.data_features[:, 17:19])

        imputer_median = SimpleImputer(missing_values=np.nan, strategy='median')
        self.data_features[:, [13, 16]] = imputer_median.fit_transform(self.data_features[:, [13, 16]])

    def preprocess(self, preprocessor):
        preprocessor.preprocess(self)
