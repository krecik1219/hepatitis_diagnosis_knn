import os
import matplotlib.pyplot as plt


class Plotter:
    def plot_and_save(self, experiment_result, directory):
        os.makedirs(directory, exist_ok=True)
        grouped_result = self._group_result(experiment_result)
        for metric, metric_groups in grouped_result.items():
            self._plot_metric_group(metric, metric_groups, directory)

    def _group_result(self, experiment_result):
        grouped_result = {}
        for avg_result in experiment_result.avg_results:
            if avg_result.metric not in grouped_result:
                grouped_result[avg_result.metric] = {}

            if avg_result.k not in grouped_result[avg_result.metric]:
                grouped_result[avg_result.metric][avg_result.k] = []

            grouped_result[avg_result.metric][avg_result.k].append((avg_result.features_num, avg_result.avg_accuracy))

        return grouped_result

    def _plot_metric_group(self, metric, metric_groups, directory):
        plot_file_name = os.path.join(directory, f'metric_{metric}.jpg')
        fig, axes = plt.subplots()  # type: (plt.Figure, plt.Axes)
        fig.set_size_inches(16, 9)
        fig.suptitle('KNN - jakość predykcji względem liczby cech')
        axes.set_title(f'metryka: {metric}')
        axes.set_xticks(range(1, 19 + 1))
        axes.set_xlabel('użyta liczba najlepszych cech')
        axes.set_ylabel('jakość predykcji')
        for k, k_group in metric_groups.items():
            features_num_with_accuracy = sorted(k_group)
            features_num_num_with_accuracy_unzipped = list(zip(*features_num_with_accuracy))
            axes.plot(features_num_num_with_accuracy_unzipped[0], features_num_num_with_accuracy_unzipped[1],
                      label=f'k: {k}', linestyle='solid', linewidth=2)
        plt.legend(loc="upper left")
        fig.savefig(plot_file_name, format='jpg', dpi=100)
