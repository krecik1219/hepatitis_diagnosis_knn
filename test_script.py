import pickle

import numpy as np
from sklearn.metrics import confusion_matrix

from data_loader import DataLoader

from features_selector import FeaturesSelector
from hepatitis_preprocessor import HepatitisPreprocessor
from experiment import Experiment, ExperimentResult
from plotter import Plotter

DATA_PATH = 'data/hepatitis.data'
LABELS_PATH = 'data/labels.txt'
CLASS_COLUMN = 0
PLOTS_DIR = './plots/'
EXPERIMENT_RESULTS_FILE = 'experiment_results.bin'
EXPERIMENT_RESULTS_INFO_FILE = 'experiment_results_info.txt'


data_loader = DataLoader(DATA_PATH, LABELS_PATH)
dataset = data_loader.load(CLASS_COLUMN)
preprocessor = HepatitisPreprocessor()
dataset.preprocess(preprocessor)
dataset.impute_missing_values()

features_selector = FeaturesSelector(dataset)
features_selector.rank_features()
labels = features_selector.get_sorted_labels('all')
values = features_selector.sorted_values
print(list(zip(labels, np.round(values, 3))))

params = {
    'metrics': ('euclidean', 'seuclidean'),
    'k': (1, 5, 10),
    'splits': 2,
    "repeats": 5,
    'features_selector': features_selector
}

best_accuracy = 0
best_metric = None
best_k = 0
best_n = 0
experiment = Experiment(**params)
avg_results = []
features_num_limit = 10
for n in range(1, len(labels) + 1):
    avg_results.extend(experiment.run(n))

experiment_result = ExperimentResult(avg_results)

best_avg_res = experiment_result.get_best_avg_result()
best_single_res = experiment_result.get_best_single_result()

plotter = Plotter()
plotter.plot_and_save(experiment_result, PLOTS_DIR)

tn, fp, fn, tp = confusion_matrix(best_single_res.get_best_single_result().y_test,
                                  best_single_res.get_best_single_result().prediction).ravel()

experiment_info_str = f'best avg result: {best_avg_res}\nbest single result (AVG): {best_single_res},\n' \
                      f'best single accuracy: {best_single_res.get_best_single_result().accuracy}\n' \
                      f'tn: {tn}, fp: {fp}, fn: {fn}, tp: {tp}\n'

print(experiment_info_str)

with open(EXPERIMENT_RESULTS_INFO_FILE, 'w') as f:
    f.write(experiment_info_str)

with open(EXPERIMENT_RESULTS_FILE, 'wb') as f:
    pickle.dump(experiment_result, f, protocol=pickle.HIGHEST_PROTOCOL)
